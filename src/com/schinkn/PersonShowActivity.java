package com.schinkn;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.schinkn.model.Person;
import com.schinkn.util.BitmapUtility;

/**
 * 
 * @author julius
 *
 */
public class PersonShowActivity extends Activity implements OnClickListener {

    private Person     person;

    private ImageView  imgView;

    private DatePicker datePicker;

    private EditText   vornameEdit, nachnameEdit, presentWishEdit;

    private CheckBox   syncBox;

    private Button     gebDatum;

    private Bitmap     img = null;

    private int        year, month, day;

    @Override
    protected void onCreate(
                            Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Get the message from the intent
        Intent intent = getIntent();
        person = MainActivity.getDatasource()
                             .getPersonWithID(( long ) intent.getExtras().get(MainActivity.CLICKED_PERSON));

        img = BitmapUtility.getPhoto(person.getImg());

        if(person.getPrename().equals("")) {
            actionBar.setTitle("Add a Person to the List");
        } else {
            actionBar.setTitle("Watching " + person.getFullName());
        }

        initLayout();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void initLayout() {

        setContentView(R.layout.person_view_detail);

        imgView = ( ImageView ) findViewById(R.id.button_image);

        if(img == null) {
            imgView.setImageResource(R.drawable.ic_person);
        } else {
            imgView.setImageBitmap(img);
        }

        vornameEdit = ( EditText ) findViewById(R.id.editText_vorname);
        nachnameEdit = ( EditText ) findViewById(R.id.editText_nachname);
        presentWishEdit = ( EditText ) findViewById(R.id.editText_gebWunsch);

        vornameEdit.setText(person.getPrename());
        nachnameEdit.setText(person.getName());
        presentWishEdit.setText(person.getPresentWish());

        gebDatum = ( Button ) findViewById(R.id.button_date);

        datePicker = new DatePicker(this);
        datePicker.setMaxDate(new Date().getTime());
        datePicker.setCalendarViewShown(true);

        syncBox = ( CheckBox ) findViewById(R.id.checkbox_sync);
        syncBox.setChecked(person.isSyncWithCalendar());

        setCurrentDateOnView();

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(
                        View v) {

        Log.d("ButtonTAG", "" + v.getTag());

        switch(v.getId()){
            case R.id.button_image:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                // photoPickerIntent.putExtra("crop", "true");
                startActivityForResult(photoPickerIntent, 1);
                break;
            case R.id.button_save:
                long id_before = person.get_id();

                person.setVorname(vornameEdit.getText().toString());
                person.setNachname(nachnameEdit.getText().toString());
                person.setGeburtsdatum(year + "-" + ( month + 1 ) + "-" + day);
                person.setPresentWish(presentWishEdit.getText().toString());
                person.setImg(BitmapUtility.getBytes( ( ( BitmapDrawable ) imgView.getDrawable() ).getBitmap()));
                person.setSyncWithCalendar(syncBox.isChecked());

                if(id_before != Person.NEW_ID) {
                    person.setId(id_before);
                }

                person = MainActivity.getDatasource().createOrUpdatePerson(person);

                Log.d("Save Person", person.toDebug());

                endActivity();
                break;
            case R.id.button_date:
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.button_cancel:
                finish();
                break;
            case R.id.button_delete:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setIcon(R.drawable.ic_action_delete);
                builder.setMessage("Are you sure?")
                       .setPositiveButton("Yes", dialogClickListener)
                       .setNegativeButton("No", dialogClickListener)
                       .show();
                break;
        }

    }

    private final static int DATE_DIALOG_ID = 99;

    @Override
    public void onActivityResult(
                                 int requestCode,
                                 int resultCode,
                                 Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1) {

            if(data != null) {

                System.out.println(data.getData());
                Uri imageUri = data.getData();
                try {
                    img = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    System.out.println(img.getClass().getName());
                    person.setImg(BitmapUtility.getBytes(img));
                    imgView.setImageBitmap(img);
                } catch(IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

        }

    }

    private void endActivity() {

        NavUtils.navigateUpFromSameTask(this);
    }

    // display current date
    public void setCurrentDateOnView() {

        if(!person.getGeburtsdatum().equals("")) {
            year = Integer.parseInt(person.getGeburtsdatum().split("-")[0]);
            month = Integer.parseInt(person.getGeburtsdatum().split("-")[1]) - 1;
            day = Integer.parseInt(person.getGeburtsdatum().split("-")[2]);
        } else {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        setDateOnButton();

    }

    private void setDateOnButton() {

        // set current date into textview
        gebDatum.setText(new StringBuilder().append(day)
                                            .append(". ")
                                            .append(PersonListAdapter.monateShort[month + 1])
                                            .append(" ")
                                            .append(year)
                                            .append(" "));// ""

        // set current date into datepicker
        datePicker.init(year, month, day, null);
    }

    @Override
    protected Dialog onCreateDialog(
                                    int id) {

        switch(id){
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener  = new DatePickerDialog.OnDateSetListener() {

                                                                       // when dialog box is closed, below method will be called.
                                                                       @Override
                                                                       public void onDateSet(
                                                                                             DatePicker view,
                                                                                             int selectedYear,
                                                                                             int selectedMonth,
                                                                                             int selectedDay) {

                                                                           day = selectedDay;
                                                                           month = selectedMonth;
                                                                           year = selectedYear;

                                                                           setDateOnButton();

                                                                       }
                                                                   };

    private DialogInterface.OnClickListener    dialogClickListener = new DialogInterface.OnClickListener() {

                                                                       @Override
                                                                       public void onClick(
                                                                                           DialogInterface dialog,
                                                                                           int which) {

                                                                           System.out.println( ( ( AlertDialog ) dialog ).getButton(which));
                                                                           switch(which){
                                                                               case DialogInterface.BUTTON_POSITIVE:
                                                                                   // Yes button clicked

                                                                                   MainActivity.getDatasource()
                                                                                               .deletePerson(person);

                                                                                   endActivity();
                                                                                   break;

                                                                               case DialogInterface.BUTTON_NEGATIVE:
                                                                                   // No button clicked
                                                                                   break;
                                                                           }
                                                                       }
                                                                   };

}
