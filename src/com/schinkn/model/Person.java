package com.schinkn.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.ContentValues;
import android.content.Intent;
import android.provider.CalendarContract.Events;
import android.text.format.Time;

public class Person implements Serializable, Comparable<Person> {

	public static final long NEW_ID = -1;

	public static final String CREATE_TABLE = "create table IF NOT EXISTS " + Person.TABLE_PERSONS + "("
			+ Person.COLUMN_ID + " integer primary key autoincrement, " + Person.COLUMN_DATE + " datetime, "
			+ Person.COLUMN_NAME + " text not null, " + Person.COLUMN_VORNAME + " text not null, " + Person.COLUMN_WISH
			+ " text not null, " + Person.COLUMN_IMG + " blob not null, " + Person.COLUMN_SYNC + " boolean not null,"
			+ Person.COLUMN_EVENT_ID + " integer not null);";

	public static final String TABLE_PERSONS = "persons";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_DATE = "datum";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_VORNAME = "vorname";
	public static final String COLUMN_WISH = "wish";
	public static final String COLUMN_IMG = "pathtoimg";
	public static final String COLUMN_SYNC = "sync";
	public static final String COLUMN_EVENT_ID = "event_id";

	// For database projection so order is consistent
	public static final String[] ALL_COLUMNS = { COLUMN_ID, COLUMN_DATE, COLUMN_NAME, COLUMN_VORNAME, COLUMN_WISH,
			COLUMN_IMG, COLUMN_SYNC, COLUMN_EVENT_ID };

	private static final long serialVersionUID = 3319806360090535737L;

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	static {
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	private long _id = Person.NEW_ID;
	private Date geburtsdatum;
	private String nachname = "", vorname = "";

	private String presentWish = "";

	private byte[] img = null;

	private boolean syncWithCalendar = false;

	private long eventId = Person.NEW_ID;

	public Person() {

	}

	@Override
	public String toString() {

		return "" + getName() + ", " + getPrename() + "(" + this.getNextAge() + ")";
		// return toDebug();
	}

	public Person(String date, String nachname, String vorname, String wish, byte[] img, boolean sync) {

		try {
			setGeburtsdatum(sdf.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.nachname = nachname;
		this.vorname = vorname;
		this.presentWish = wish;
		this.img = img;
		this.syncWithCalendar = sync;
	}

	public long get_id() {

		return _id;
	}

	public void setId(long _id) {

		this._id = _id;
	}

	public Date getGeburtsDatum() {

		return geburtsdatum;
	}

	public String getGeburtsdatum() {

		if (geburtsdatum == null) {
			return "";
		}
		return sdf.format(geburtsdatum);
	}

	public void setGeburtsdatum(Date geburtsdatum) {

		this.geburtsdatum = geburtsdatum;
	}

	public void setGeburtsdatum(String date) {

		try {
			this.geburtsdatum = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getName() {

		return nachname;
	}

	public void setNachname(String nachname) {

		this.nachname = nachname;
	}

	public String getPrename() {

		return vorname;
	}

	public void setVorname(String vorname) {

		this.vorname = vorname;
	}

	public String getPresentWish() {

		return presentWish;
	}

	public String getDescription() {

		if (presentWish.equals("")) {
			return "Wishes for:";
		}
		return "Wishes for:\n" + presentWish;
	}

	public void setPresentWish(String presentWish) {

		this.presentWish = presentWish;
	}

	public boolean isSyncWithCalendar() {

		return syncWithCalendar;
	}

	public void setSyncWithCalendar(boolean syncWithCalendar) {

		this.syncWithCalendar = syncWithCalendar;
	}

	public String toDebug() {

		return "Person [_id=" + _id + ", geburtsdatum=" + getGeburtsdatum() + ", nachname=" + nachname + ", vorname="
				+ vorname + ", presentWish=" + getPresentWish() + ", img=" + img + ", syncWithCalendar="
				+ syncWithCalendar + ", event_id=" + eventId + "]";
	}

	public byte[] getImg() {

		return img;
	}

	public void setImg(byte[] imgByte) {

		this.img = imgByte;
	}

	public int getNextAge() {

		Calendar dob = Calendar.getInstance();
		dob.setTime(geburtsdatum);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}
		return age + 1;
	}

	public String getFullName() {

		if (vorname.equals("")) {
			return nachname;
		}
		if (nachname.equals("")) {
			return vorname;
		}
		return vorname + " " + nachname;
	}

	public long getBirthdayInMillis() {

		return geburtsdatum.getTime();
	}

	public boolean isEmpty() {

		if (nachname.equals("") && vorname.equals("") && presentWish.equals("")) {
			return true;
		}
		return false;
	}

	@Override
	public int compareTo(Person another) {

		return this.getFullName().compareTo(another.getFullName());
	}

	public String getDateStamp() {

		Calendar cal = Calendar.getInstance();
		cal.setTime(geburtsdatum);
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		System.out.println(cal.getTime());

		return geburtsdatum + " vs. " + getGeburtsdatum();
	}

	public long getEventId() {

		return eventId;
	}

	public void setEventId(long eventId) {

		this.eventId = eventId;
	}

	public ContentValues getContentValues() {

		return new CalendarEvent(this.getBirthdayInMillis(), this.getFullName(), "FREQ=YEARLY;INTERVAL=1",
				"W�nscht sich:\n" + this.getPresentWish(), Time.TIMEZONE_UTC, true).getValues();
	}

	public ContentValues getContentValues(long calID) {

		return new CalendarEvent(this.getBirthdayInMillis(), this.getFullName(), "FREQ=YEARLY;INTERVAL=1",
				this.getDescription(), Time.TIMEZONE_UTC, true).getValues(calID);
	}

	public Intent getIntentValues(Intent intent, long calID) {

		intent.setType("vnd.android.cursor.item/event");
		intent.putExtra("beginTime", getBirthdayInMillis());
		intent.putExtra(Events.ALL_DAY, true);
		intent.putExtra(Events.RRULE, "FREQ=YEARLY");
		intent.putExtra("endTime", getBirthdayInMillis());
		intent.putExtra(Events.TITLE, getFullName());
		intent.putExtra(Events.DESCRIPTION, getDescription());
		intent.putExtra(Events.EVENT_LOCATION, String.valueOf(get_id()));
		return intent;
	}

	public String toXMLString() {

		StringBuilder imgString = new StringBuilder(img.toString());

		// for(byte b : img) {
		// imgString.append(b + ",");
		// }

		StringBuilder xmlBuilder = new StringBuilder("<person>");

		xmlBuilder.append("<prename> " + getPrename() + " </prename>\n");
		xmlBuilder.append("<name> " + getName() + " </name>\n");
		xmlBuilder.append("<birthday> " + getBirthdayInMillis() + " </birthday>\n");
		xmlBuilder.append("<picture> " + imgString + " </picture>\n");
		xmlBuilder.append("<sync> " + (isSyncWithCalendar() ? 1 : 0) + " </sync>\n");
		xmlBuilder.append("<eventid> " + getEventId() + " </eventid>\n");

		xmlBuilder.append("</person>\n");

		return xmlBuilder.toString();
	}

}
