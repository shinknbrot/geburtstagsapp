package com.schinkn.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

import com.schinkn.model.Person;

public class PersonUtility {
    public static boolean exportPersonsToXML(
                                             ArrayList<Person> pList) {

        StringBuilder xmlBuilder = new StringBuilder();

        xmlBuilder.append("<liste>");
        for(Person person : pList) {

            xmlBuilder.append(person.toXMLString());
        }
        xmlBuilder.append("</liste>");

        System.out.println(xmlBuilder);
        return true;
    }

    public static void test() {

        XmlPullParser xml = Xml.newPullParser();

        try {
            xml.setInput(new FileInputStream(new File("")), null);

        } catch(FileNotFoundException | XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
